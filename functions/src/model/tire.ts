export class Tire {
    ETRTO: string;
    UK: string;
    FR: string;
    image: string;

    constructor(ETRTO: string, UK: string, FR: string, image?: string) {
        this.ETRTO = ETRTO;
        this.UK = UK;
        this.FR = FR;
        this.image = image === undefined ? '' : image;
    }
}
