export class Pressure {
    Bar: number;
    PSI: number;


    constructor(Bar: number, PSI: number) {
        this.Bar = Bar;
        this.PSI = PSI;
    }
}
