import {Tire} from "./model/tire";
import * as data from "./data.json";
import * as pressures from "./data/coresspondancePressionLargeur.json";
import {Pressure} from "./model/pressure";

type MapOfPouces = Record<string, Tire[]>;
type MapOfPressure = Record<number, Pressure>;

// @ts-ignore
const mapOfPouces: MapOfPouces = data;
// @ts-ignore
const mapOfPressure: MapOfPressure = pressures;


export class TireService {
    getAllIsoForPouce(pouce: string): Tire[] {
        let listOfIsos: Tire[] = [];
        mapOfPouces[pouce].forEach(iso => {
            listOfIsos.push(iso);
        });
        return listOfIsos;
    }

    getAllIso(): Tire[] {
        let listOfIsos: Tire[] = [];
        for (let pouce in mapOfPouces) {
            mapOfPouces[pouce].forEach(iso => {
                listOfIsos.push(iso);
            });
        }
        return listOfIsos;
    }

    getTenIsosForPouce(pouce: string, offset: number): Tire[] {
        return this.getAllIsoForPouce(pouce).slice(offset, offset + 9)
    }

    getTire(ETRTO: string): Tire {
        let tire = this.getAllIso().find(function (element: Tire) {
            return element.ETRTO === ETRTO;
        });
        return tire === undefined ? new Tire('', '', '') : tire;
    }

    getPressionForETRTO(ETRTO: string): Pressure {
        const TIRE = this.getTire(ETRTO);
        const WIDTH = parseInt(TIRE.ETRTO.substring(0, 2));
        const PRESSURE = mapOfPressure[WIDTH];
        return PRESSURE;
    }
}