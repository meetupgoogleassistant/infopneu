import * as functions from 'firebase-functions';
import {BrowseCarouselItem, dialogflow, DialogflowConversation, BrowseCarousel, Suggestions} from "actions-on-google";
import {TireService} from "./service";
import {Tire} from "./model/tire";
import {CONTEXT} from "./model/context";


const APP = dialogflow({
    debug: true
});

const SERVICE = new TireService();

exports.bikeTireFulfillment = functions.https.onRequest(APP);

function welcome(conversation: any) {
    conversation.ask("Hello World !!!");
    return conversation
}

function taillePneuIntent(conversation: DialogflowConversation) {
    conversation.ask("Hello taillePneuIntent !!!");
    // @ts-ignore
    const SIZE = conversation.body.queryResult.parameters.number;
    let isos: Tire[];
    conversation.user.storage.offset = 10;
    conversation.user.storage.size = SIZE;
    isos = SERVICE.getTenIsosForPouce(SIZE, 0);

    let answer = "Les pneus 26 pouces sont :";
    isos.forEach(iso => {
        answer += `, ${iso.ETRTO}`
    });
    conversation.ask(answer);

    if (conversation.screen
        || conversation.surface.capabilities.has('actions.capability.WEB_BROWSER')) {


        let carousselsItems: BrowseCarouselItem[] = [];
        isos.forEach(iso => {
            const ITEM = new BrowseCarouselItem({
                title: iso.ETRTO,
                url: `https://www.google.com/search?q=pneu+${iso.ETRTO}`,
                description: `Dimension UK: ${iso.UK}, Dimensions française: ${iso.FR}`,
                footer: 'Item 1 footer',
            });
            carousselsItems.push(ITEM);
        });

        conversation.ask(new BrowseCarousel({
            items: [...carousselsItems],
        }));
    }

    conversation.ask(new Suggestions(['Plus de résultats', `pression pneu ${isos[0].ETRTO}`]));
    conversation.contexts.set(CONTEXT.PRESSION_PNEU_INTENT_FOLLOWUP, 5);
    return;
}


function moreTaillePneuIntent(conversation: DialogflowConversation) {
    const OFFSET = conversation.user.storage.offset;
    const SIZE = conversation.user.storage.size;

    let isos: Tire[] = SERVICE.getTenIsosForPouce(SIZE, OFFSET);
    if(OFFSET === 0 && isos.length === 10){
        conversation.user.storage.offset = OFFSET + 10;
    }
    else {
        conversation.user.storage.offset = 0 ;
    }
    let answer = "";
    isos.forEach(iso => {
        answer += `${iso.ETRTO} ,`;

    });
    conversation.ask(answer);
    conversation.ask(new Suggestions(['Plus de résultats', `pression pneu ${isos[0].ETRTO}`]));
    conversation.contexts.set(CONTEXT.PRESSION_PNEU_INTENT_FOLLOWUP, 5);
    return;
}
function pressionPneuIntent(conversation: DialogflowConversation) {
    // @ts-ignore
    const ETRTO = conversation.body.queryResult.parameters.dimensionPneus;
    console.log(ETRTO);
    const TIRE = SERVICE.getPressionForETRTO(ETRTO);
    console.log(TIRE);

    let answer = `Pour un pneu ${ETRTO} vous devez gonfler votre pneu à ${TIRE.Bar} bar ou ${TIRE.PSI} PSI`;
    conversation.ask(answer);
    return;

}

APP.intent('Default Welcome Intent', welcome);
// @ts-ignore
APP.intent('taillePneuIntent', taillePneuIntent);
// @ts-ignore
APP.intent('pressionPneuIntent', pressionPneuIntent);
// @ts-ignore
APP.intent('taillePneuIntent - more', moreTaillePneuIntent);

